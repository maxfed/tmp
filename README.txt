1) git clone https://github.com/davletov-aa/mcl-wic
2) pip install -r requirements.txt
3) download checkpoints and datasets (https://drive.google.com/drive/folders/1IhXezvjl30ypXqfoeraOm6Zv7MpI3Odr?usp=sharing)
4) bash run_pipeline.sh


reproduce test submissions:
best linear_regression_model: pytorch_model_by_scd_2.bin (https://drive.google.com/drive/folders/1LCsQyE193EgmaJCZG1AlI593sEeoXLuC)
trained on train_1 + train_2. del_last_and_first=True, del_short_and_long=True, n_pair=100

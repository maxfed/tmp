import pandas as pd
import os
import sys
import argparse

def search_lem(word, r):
    positions = []
    start = 0
    end = 0
    for i, j in zip(r.lemms, r.words):
        if word == i:
            start = r.sent.find(j, end)
            end = start + len(j)
            positions.append((start, end))
    if len(positions) == 0:
        positions = 0
    return positions


def search_and_save(word, df, mode):
    #df.sent - предложение из исходного корпуса
    #df.words - 1 столбец из того, что выдал лемматизатор(то, что было ему подано)
    #df.lemms - 2 столбец из того, что выдал лемматизатор(леммы)
    df['positions'] = df.apply(lambda r: search_lem(word, r), axis=1)
    new_df = df[df['positions'] != 0]
    new_df = new_df.drop(['words', 'lemms'], axis=1)
    new_df.to_csv(str(word) + str(mode) + '.tsv', sep='\t', index=False)

def converting(text, lemms_text):
    sents = text.split('\n')
    sents = [i for i in sents if i != ""]
    words = []
    lemms = []
    all_words = []
    all_lemms = []
    lt = lemms_text.split('\n')
    for i in lt:
        if i == '':
            if words != []:
                all_words.append(words)
            if lemms != []:
                all_lemms.append(lemms)
            words = []
            lemms = []
        else:
            q = i.split('\t')
            words.append(q[1])
            lemms.append(q[2])
    df = pd.DataFrame()
    df['sent'] = sents
    df['words'] = all_words
    df['lemms'] = all_lemms
    return df


parser = argparse.ArgumentParser()
parser.add_argument("--text_file")
parser.add_argument("--lem_file")
parser.add_argument("--words")
args = parser.parse_args()
#input_file = sys.argv[2]
#lem_file = sys.argv[4]
all_input_file = args.text_file.replace(' ', '').strip('[]').split(',')
all_lem_file = args.lem_file.replace(' ', '').strip('[]').split(',')
words = args.words.replace(' ', '').strip('[]').split(',')
for input_file, lem_file in zip(all_input_file, all_lem_file):
    ftext = open(input_file)
    flem = open(lem_file)
    mode = 2
    if 'post' in input_file:
        mode = 3
    elif 'pre' in input_file:
        mode = 1
    text = ftext.read()
    lemms_text = flem.read()
    df = converting(text, lemms_text)
    for word in words:
        search_and_save(word, df, mode)

'''"[out_file.txt]"
"[new_file_ans20.conllu]"
"[основной]"'''
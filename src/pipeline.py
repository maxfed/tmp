import pandas as pd
import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LinearRegression
from sklearn.isotonic import IsotonicRegression
from src.metrics import spearman
import fire

def FitIsotonicRegression(X_train, y_train, X_test, out_of_bounds='clip', verbose=False):
    iso_reg = IsotonicRegression(y_min=1., y_max=4., increasing='auto', out_of_bounds=out_of_bounds).fit(X_train, y_train)
    if verbose:
        y_pred = iso_reg.predict(X_train)
        corr, _ = spearmanr(y_pred, y_train)
        corr_wo_train, _ = spearmanr(X_train, y_train)
        print(f'Corr on train = {corr:.4f}, w/o isotonic regr = {corr_wo_train:.4f}')
    return iso_reg.predict(X_test)

def iso_method(path, train=['df_train'], dev=['df_dev_1', 'df_dev_2'], test=['df_test_sample_12', 'df_test_sample_13']):
    pass

def statistics_method(path, train=['df_train'], dev=['df_dev_1', 'df_dev_2'], test=['df_test_sample_12', 'df_test_sample_13'], stat=['mean', '0.25', '0.5', '0.75']):
    #stat: 0)mean  1)numbers of quantils, ['0.25', '0.5'] default = ['mean', '0.25', '0.5', '0.75']
    df = pd.read_csv(path, sep='\t')
    df_ans1 = pd.read_csv('testset1.tsv', sep='\t')
    df_ans2 = pd.read_csv('testset2.tsv', sep='\t')
    df_ans1 = df_ans1.drop(
        columns=['EARLIER', 'LATER', 'delta_later', 'frequency_earlier', 'frequency_later', 'delta_frequency'])
    df_ans2 = df_ans2.drop(
        columns=['EARLIER', 'LATER', 'delta_later', 'frequency_earlier', 'frequency_later', 'delta_frequency'])
    df_train = df[df.part == 'train.original_scd_12_23']
    df_dev_1 = df[df.part == 'dev.original_scd_12']
    df_dev_2 = df[df.part == 'dev.original_scd_23']
    df_dev_sample_1 = df[df.part == 'dev.true_true_sampled_scd_12']
    df_dev_sample_2 = df[df.part == 'dev.true_true_sampled_scd_23']
    df_train_sample_1 = df[df.part == 'train.true_true_sampled_scd_12']
    df_train_sample_2 = df[df.part == 'train.true_true_sampled_scd_23']
    df_test_sample_12 = df[df.part == 'test.true_true_sampled_scd_12']
    df_test_sample_13 = df[df.part == 'test.true_true_sampled_scd_13']
    df_test_sample_23 = df[df.part == 'test.true_true_sampled_scd_23']
    dfs = {'df_train': df_train, 'df_dev_1': df_dev_1, 'df_dev_2': df_dev_2, 'df_dev_sample_1': df_dev_sample_1,
           'df_dev_sample_2': df_dev_sample_2, 'df_train_sample_1': df_train_sample_1,
           'df_train_sample_2': df_train_sample_2, 'df_test_sample_12': df_test_sample_12,
           'df_test_sample_13': df_test_sample_13,
           'df_test_sample_23': df_test_sample_23}
    df_s = {}
    df_subm = pd.DataFrame()
    dfs_stat = {}
    for i in dfs:
        for i1 in stat:
            if i1 == 'mean':
                dfs_stat[i1] = dfs[i].groupby('word').mean().reset_index()
            else:
                dfs_stat[i1] = dfs[i].groupby('word').quantile(q=float(i1)).reset_index()
        #df_mean = dfs[i].groupby('word').mean().reset_index()
        #df_q25 = dfs[i].groupby('word').quantile(q=0.25).reset_index()
        #df_q5 = dfs[i].groupby('word').quantile(q=0.5).reset_index()
        #df_q75 = dfs[i].groupby('word').quantile(q=0.75).reset_index()
        ddf = pd.DataFrame()
        ddf['word'] = dfs_stat[stat[0]]['word']
        if i == 'df_train' or i == 'df_dev_1' or i == 'df_dev_2':
            ddf['gold_score'] = dfs_stat[stat[0]]['gold_score']
        elif 'dev' in i or 'train' in i:
            gold_scores = []
            for j in list(dfs_stat[stat[0]].word):
                if '1' in i:
                    gold_scores.append(list(df_ans1[df_ans1['word'] == j].COMPARE)[0])
                elif '2' in i:
                    gold_scores.append(list(df_ans2[df_ans2['word'] == j].COMPARE)[0])
            ddf['gold_score'] = gold_scores
        for j in dfs_stat[stat[0]].columns:
            if j != 'word' and j != 'score' and j != 'gold_score':
                pp = []
                for q in dfs_stat:
                    pp.append(list(dfs_stat[q][j]))
                aa = []
                for q1 in range(len(pp[0])):
                    list_stat = []
                    for q2 in range(len(pp)):
                        list_stat.append(pp[q2][q1])
                    aa.append(list_stat)
                ddf[j] = aa
                #a = list(df_mean[j])
                #b = list(df_q25[j])
                #c = list(df_q5[j])
                #d = list(df_q75[j])
                #aa = []
                #for q in range(len(a)):
                #    # aa.append([a[q], c[q]])
                #    aa.append([a[q], b[q], c[q], d[q]])
                #    # aa.append(a[q])
                #ddf[j] = aa
        df_s[i] = ddf

    df_all_scores = pd.DataFrame()
    se = list(df_s['df_train'].columns)

    se = [s for s in se if s != 'word' and s != 'score' and s != 'gold_score']
    df_all_scores['model'] = se
    corrs = {}
    all_corrs = []
    ww = list(df_s['df_test_sample_12']['word'])
    for i in se:
        X_train = []
        y_train = []
        X_dev = []
        y_dev = []
        X_test = []
        preds_dev = []
        preds_test = []
        for tr in train:
            x = list(df_s[tr][i])
            y = list(df_s[tr]['gold_score'])
            X_train.extend(x)
            y_train.extend(y)

        for dv in dev:
            x = list(df_s[dv][i])
            y = list(df_s[dv]['gold_score'])
            X_dev.append(x)
            y_dev.append(y)

        for ts in test:
            x = list(df_s[ts][i])
            X_test.append(x)
        lr = LinearRegression()
        parameters = {}
        reg = GridSearchCV(lr, parameters, cv=10)
        reg.fit(X_train, y_train)
        for dv in X_dev:
            pred_dev = reg.predict(dv)
            preds_dev.append(pred_dev)
        for ts in X_test:
            pred_test = reg.predict(ts)
            preds_test.append(pred_test)
        for dv, (pred_d, y_d) in zip(dev, zip(preds_dev, y_dev)):
            cor = spearman(pred_d, y_d)
            if dv not in corrs:
                corrs[dv] = []
            corrs[dv].append(cor)
        for ts, pred_test in zip(test, preds_test):
            df_subm['word'] = ww
            df_subm[ts] = pred_test

    for cor in corrs:
        df_all_scores[cor] = corrs[cor]

    df_all_scores.to_csv('name', sep='\t', index=False)
    print(df_all_scores)
    print(df_subm)

if __name__ == '__main__':
    fire.Fire(statistics_method)


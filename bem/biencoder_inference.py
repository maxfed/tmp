'''
Copyright (c) Facebook, Inc. and its affiliates.
All rights reserved.

This source code is licensed under the license found in the
LICENSE file in the root directory of this source tree.
'''

from nltk.corpus import wordnet as wn
from nltk.tokenize import word_tokenize
import argparse
import json
import numpy as np
from tqdm import tqdm

from bem.wsd_models.util import *
from bem.wsd_models.models import BiEncoderModel

parser = argparse.ArgumentParser(description='Gloss Informed Bi-encoder for WSD')

#training arguments
parser.add_argument('--rand_seed', type=int, default=42)
# parser.add_argument('--grad-norm', type=float, default=1.0)
# parser.add_argument('--silent', action='store_true',
# 	help='Flag to supress training progress bar for each epoch')
parser.add_argument('--multigpu', action='store_true')
# parser.add_argument('--lr', type=float, default=0.00001)
# parser.add_argument('--warmup', type=int, default=10000)
parser.add_argument('--context-max-length', type=int, default=128)
parser.add_argument('--gloss-max-length', type=int, default=32)
# parser.add_argument('--epochs', type=int, default=20)
parser.add_argument('--context-bsz', type=int, default=4)
parser.add_argument('--gloss-bsz', type=int, default=256)
parser.add_argument('--encoder-name', type=str, default='bert-base',
	choices=['bert-base', 'bert-large', 'roberta-base', 'roberta-large', 'xlm-roberta-base', 'xlm-roberta-large'])
parser.add_argument('--data-path', type=str, required=True,
	help='Location of top-level directory for the Unified WSD Framework')

#sets which parts of the model to freeze ❄️ during training for ablation
parser.add_argument('--freeze-context', action='store_true')
parser.add_argument('--freeze-gloss', action='store_true')
parser.add_argument('--tie-encoders', action='store_true')

#other training settings flags
# parser.add_argument('--kshot', type=int, default=-1,
# 	help='if set to k (1+), will filter training data to only have up to k examples per sense')
# parser.add_argument('--balanced', action='store_true',
# 	help='flag for whether or not to reweight sense losses to be balanced wrt the target word')

#evaluation arguments
# parser.add_argument('--eval', action='store_true',
# 	help='Flag to set script to evaluate probe (rather than train)')
# parser.add_argument('--split', type=str, default='semeval2007',
# 	choices=['semeval2007', 'senseval2', 'senseval3', 'semeval2013', 'semeval2015', 'ALL', 'all-test'],
# 	help='Which evaluation split on which to evaluate probe')
parser.add_argument('--samples-data-path', type=str, required=True,
	help='Location of the samples data')
parser.add_argument('--samples-data-format', type=str, default='single',
	choices=['wic', 'single'])
parser.add_argument('--only-context-output', action='store_true')
parser.add_argument('--max-context-words-count', type=int, default=None)
parser.add_argument('--path-to-model', type=str, default=None,
	help='Path to saved model')
parser.add_argument('--epoch-model-format', action='store_true')
parser.add_argument('--output-directory', type=str, required=True,
	help='Where to store predictions')

#uses these two gpus if training in multi-gpu
context_device = "cuda:0"
gloss_device = "cuda:1"
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def tokenize_glosses(gloss_arr, tokenizer, max_len):
	glosses = []
	masks = []
	for gloss_text in gloss_arr:
		g_ids = [torch.tensor([[x]]) for x in tokenizer.encode(tokenizer.cls_token)+tokenizer.encode(gloss_text)+tokenizer.encode(tokenizer.sep_token)]
		g_attn_mask = [1]*len(g_ids)
		g_fake_mask = [-1]*len(g_ids)
		g_ids, g_attn_mask, _ = normalize_length(g_ids, g_attn_mask, g_fake_mask, max_len, pad_id=tokenizer.encode(tokenizer.pad_token)[0])
		g_ids = torch.cat(g_ids, dim=-1)
		g_attn_mask = torch.tensor(g_attn_mask)
		glosses.append(g_ids)
		masks.append(g_attn_mask)

	return glosses, masks

#creates a sense label/ gloss dictionary for training/using the gloss encoder
def load_and_preprocess_glosses(data, tokenizer, wn_senses, max_len=-1):
	sense_glosses = {}
	sense_weights = {}

	for sent in data:
		for _, lemma, pos, _, label in sent:
			if not label:
				continue #ignore unlabeled words
			else:
				key = generate_key(lemma, pos)
				if key not in sense_glosses:
					#get all sensekeys for the lemma/pos pair
					if key not in wn_senses:
						key = key.lower()
					sensekey_arr = wn_senses[key]
					#get glosses for all candidate senses
					gloss_arr = [wn.lemma_from_key(s).synset().definition() for s in sensekey_arr]

					#preprocess glosses into tensors
					gloss_ids, gloss_masks = tokenize_glosses(gloss_arr, tokenizer, max_len)
					gloss_ids = torch.cat(gloss_ids, dim=0)
					gloss_masks = torch.stack(gloss_masks, dim=0)
					sense_glosses[key] = (gloss_ids, gloss_masks, sensekey_arr)

					#intialize weights for balancing senses
					# sense_weights[key] = [0]*len(gloss_arr)
					# w_idx = sensekey_arr.index(label)
					# sense_weights[key][w_idx] += 1
				# else:
					#update sense weight counts
					# w_idx = sense_glosses[key][2].index(label)
					# sense_weights[key][w_idx] += 1

				#make sure that gold label is retrieved synset
				# assert label in sense_glosses[key][2]

	#normalize weights
	# for key in sense_weights:
	# 	total_w = sum(sense_weights[key])
	# 	sense_weights[key] = torch.FloatTensor([total_w/x if x !=0 else 0 for x in sense_weights[key]])

	return sense_glosses, sense_weights

def preprocess_context(tokenizer, text_data, bsz=1, max_len=-1):
	if max_len == -1: assert bsz==1 #otherwise need max_length for padding

	context_ids = []
	context_attn_masks = []

	example_keys = []

	context_output_masks = []
	instances = []
	labels = []

	#tensorize data
	for sent in text_data:
		c_ids = [torch.tensor([tokenizer.encode(tokenizer.cls_token)])] #cls token aka sos token, returns a list with index
		o_masks = [-1]
		sent_insts = []
		sent_keys = []
		sent_labels = []

		#For each word in sentence...
		for idx, (word, lemma, pos, inst, label) in enumerate(sent):
			#tensorize word for context ids
			word_ids = [torch.tensor([[x]]) for x in tokenizer.encode(word.lower())]
			c_ids.extend(word_ids)

			#if word is labeled with WSD sense...
			if inst != -1:
				#add word to bert output mask to be labeled
				o_masks.extend([idx]*len(word_ids))
				#track example instance id
				sent_insts.append(inst)
				#track example instance keys to get glosses
				ex_key = generate_key(lemma, pos)
				sent_keys.append(ex_key)
				sent_labels.append(label)
			else:
				#mask out output of context encoder for WSD task (not labeled)
				o_masks.extend([-1]*len(word_ids))

			#break if we reach max len
			if max_len != -1 and len(c_ids) >= (max_len-1):
				break

		c_ids.append(torch.tensor([tokenizer.encode(tokenizer.sep_token)])) #aka eos token
		c_attn_mask = [1]*len(c_ids)
		o_masks.append(-1)
		c_ids, c_attn_masks, o_masks = normalize_length(c_ids, c_attn_mask, o_masks, max_len, pad_id=tokenizer.encode(tokenizer.pad_token)[0])

		y = torch.tensor([1]*len(sent_insts), dtype=torch.float)
		#not including examples sentences with no annotated sense data
		if len(sent_insts) > 0:
			context_ids.append(torch.cat(c_ids, dim=-1))
			context_attn_masks.append(torch.tensor(c_attn_masks).unsqueeze(dim=0))
			context_output_masks.append(torch.tensor(o_masks).unsqueeze(dim=0))
			example_keys.append(sent_keys)
			instances.append(sent_insts)
			labels.append(sent_labels)

	#package data
	data = list(zip(context_ids, context_attn_masks, context_output_masks, example_keys, instances, labels))

	#batch data if bsz > 1
	if bsz > 1:
		print('Batching data with bsz={}...'.format(bsz))
		batched_data = []
		for idx in range(0, len(data), bsz):
			if idx+bsz <=len(data): b = data[idx:idx+bsz]
			else: b = data[idx:]
			context_ids = torch.cat([x for x,_,_,_,_,_ in b], dim=0)
			context_attn_mask = torch.cat([x for _,x,_,_,_,_ in b], dim=0)
			context_output_mask = torch.cat([x for _,_,x,_,_,_ in b], dim=0)
			example_keys = []
			for _,_,_,x,_,_ in b: example_keys.extend(x)
			instances = []
			for _,_,_,_,x,_ in b: instances.extend(x)
			labels = []
			for _,_,_,_,_,x in b: labels.extend(x)
			batched_data.append((context_ids, context_attn_mask, context_output_mask, example_keys, instances, labels))
		return batched_data
	else:
		return data

def _eval(eval_data, model, gloss_dict, multigpu=False):
	model.eval()
	eval_preds = []
	for context_ids, context_attn_mask, context_output_mask, example_keys, insts, _ in tqdm(eval_data):
		with torch.no_grad():
			#run example through model
			if multigpu:
				context_ids = context_ids.to(context_device)
				context_attn_mask = context_attn_mask.to(context_device)
			else:
				context_ids = context_ids.to(device)
				context_attn_mask = context_attn_mask.to(device)
			context_output = model.context_forward(context_ids, context_attn_mask, context_output_mask)

			for output, key, inst in zip(context_output.split(1,dim=0), example_keys, insts):
				#run example's glosses through gloss encoder
				if key not in gloss_dict:
					key = key.lower()
				gloss_ids, gloss_attn_mask, sense_keys = gloss_dict[key]
				if multigpu:
					gloss_ids = gloss_ids.to(gloss_device)
					gloss_attn_mask = gloss_attn_mask.to(gloss_device)
				else:
					gloss_ids = gloss_ids.to(device)
					gloss_attn_mask = gloss_attn_mask.to(device)
				gloss_output = model.gloss_forward(gloss_ids, gloss_attn_mask)
				gloss_output = gloss_output.transpose(0,1)
				current_context_output = output.squeeze().detach().cpu().tolist()

				#get cosine sim of example from context encoder with gloss embeddings
				if multigpu:
					output = output.cpu()
					gloss_output = gloss_output.cpu()
				output = torch.mm(output, gloss_output)
				pred_idx = output.topk(1, dim=-1)[1].squeeze().item()
				pred_label = sense_keys[pred_idx]
				probs = output.squeeze().detach().cpu().numpy()
				labels_probs = {sense_keys[i]: float(probs[i]) for i in range(len(sense_keys))}
				sample_result = {
					'sample_id': inst, 'pred_label': pred_label,
					'probs': labels_probs, 'context_output': current_context_output,
				}
				eval_preds.append(sample_result)

	return eval_preds


def _eval_context(eval_data, model, multigpu=False):
	model.eval()
	eval_preds = []
	for context_ids, context_attn_mask, context_output_mask, example_keys, insts, _ in tqdm(eval_data):
		with torch.no_grad():
			#run example through model
			if multigpu:
				context_ids = context_ids.to(context_device)
				context_attn_mask = context_attn_mask.to(context_device)
			else:
				context_ids = context_ids.to(device)
				context_attn_mask = context_attn_mask.to(device)
			context_output = model.context_forward(context_ids, context_attn_mask, context_output_mask)

			for output, key, inst in zip(context_output.split(1,dim=0), example_keys, insts):
				current_context_output = output.squeeze().detach().cpu().tolist()
				sample_result = {
					'sample_id': inst, 'context_output': current_context_output,
				}
				eval_preds.append(sample_result)

	return eval_preds


def construct_sample(left_context, word, right_context, sample_id, target_lemma, pos, max_context_words_count=None):
	left_tokens = word_tokenize(left_context)
	right_tokens = word_tokenize(right_context)

	def get_context_token_info(token):
		return token, '', -1, -1, False

	left_tokens = [get_context_token_info(token) for token in left_tokens]
	right_tokens = [get_context_token_info(token) for token in right_tokens]
	target_token = (word, target_lemma, pos, sample_id, True)

	if max_context_words_count is None or len(left_tokens) + len(right_tokens) <= max_context_words_count:
		return left_tokens + [target_token] + right_tokens

	shift = max_context_words_count // 2
	left_remainder = max(0, shift - len(left_tokens))
	right_remainder = max(0, shift - len(right_context))

	return left_tokens[-(shift + right_remainder):] + [target_token] + right_tokens[:shift + left_remainder]


def parse_wic_sample(wic_sample, max_context_words_count=None):
	start_1 = int(wic_sample['start1'])
	end_1 = int(wic_sample['end1'])
	sample_1_id = wic_sample['id'] + '#1'
	wsd_sample_1 = construct_sample(left_context=wic_sample['sentence1'][:start_1],
									word=wic_sample['sentence1'][start_1:end_1],
									right_context=wic_sample['sentence1'][end_1:],
									sample_id=sample_1_id,
									target_lemma=wic_sample['lemma'], pos=wic_sample['pos'],
									max_context_words_count=max_context_words_count)

	start_2 = int(wic_sample['start2'])
	end_2 = int(wic_sample['end2'])
	sample_2_id = wic_sample['id'] + '#2'
	wsd_sample_2 = construct_sample(left_context=wic_sample['sentence2'][:start_2],
									word=wic_sample['sentence2'][start_2:end_2],
									right_context=wic_sample['sentence2'][end_2:],
									sample_id=sample_2_id,
									target_lemma=wic_sample['lemma'], pos=wic_sample['pos'],
									max_context_words_count=max_context_words_count)

	return wic_sample['id'], [wsd_sample_1, wsd_sample_2]

def merge_wsd_predictions(wic_ids, wsd_preds):
	def clean_pred_from_id(pred):
		return {key: value for key, value in pred.items() if key != 'sample_id'}

	wic_predictions = []
	wsd_preds_dict = {pred['sample_id']: clean_pred_from_id(pred) for pred in wsd_preds}

	for wic_id in wic_ids:
		pred_1 = wsd_preds_dict[wic_id + '#1']
		pred_2 = wsd_preds_dict[wic_id + '#2']
		current_wic_prediction = {'id': wic_id}

		for key, value in pred_1.items():
			current_wic_prediction[key + '1'] = value
		for key, value in pred_2.items():
			current_wic_prediction[key + '2'] = value

		wic_predictions.append(current_wic_prediction)

	return wic_predictions


def transform_wic_data(wic_data, max_context_words_count=None):
	wic_ids, wsd_data = [], []

	for wic_sample in wic_data:
		wic_id, samples_pair = parse_wic_sample(wic_sample, max_context_words_count=max_context_words_count)
		wsd_data.extend(samples_pair)
		wic_ids.append(wic_id)

	return wic_ids, wsd_data


def load_wic_data(data_path, max_context_words_count=None):
	with open(data_path, 'r') as f:
		data_json = json.load(f)
		return transform_wic_data(data_json, max_context_words_count=max_context_words_count)


def load_single_data(data_path, max_context_words_count=None):
	with open(data_path, 'r') as f:
		data_json = json.load(f)

	wsd_data = []
	for data_sample in data_json:
		start = int(data_sample['start'])
		end = int(data_sample['end'])
		sample = construct_sample(
			left_context=data_sample['sentence'][:start],
			word=data_sample['sentence'][start:end],
			right_context=data_sample['sentence'][end:],
			sample_id=data_sample['id'],
			target_lemma=data_sample['lemma'], pos=data_sample['pos'],
			max_context_words_count=max_context_words_count
		)
		wsd_data.append(sample)

	return wsd_data


def get_wsd_preds(args, wsd_eval_data, model, tokenizer):
	if not args.only_context_output:
		#load gloss dictionary (all senses from wordnet for each lemma/pos pair that occur in data)
		wn_path = os.path.join(args.data_path, 'Data_Validation/candidatesWN30.txt')
		wn_senses = load_wn_senses(wn_path)
		gloss_dict, _ = load_and_preprocess_glosses(wsd_eval_data, tokenizer, wn_senses, max_len=32)
		wsd_eval_data = preprocess_context(tokenizer, wsd_eval_data, bsz=1, max_len=-1)
		eval_wsd_preds = _eval(wsd_eval_data, model, gloss_dict, multigpu=False)
	else:
		wsd_eval_data = preprocess_context(tokenizer, wsd_eval_data, bsz=1, max_len=-1)
		eval_wsd_preds = _eval_context(wsd_eval_data, model, multigpu=False)

	return eval_wsd_preds


def get_wic_preds(args, model, tokenizer):
	wic_ids, wsd_eval_data = load_wic_data(args.samples_data_path, max_context_words_count=args.max_context_words_count)
	eval_wsd_preds = get_wsd_preds(args, wsd_eval_data, model, tokenizer)

	return merge_wsd_predictions(wic_ids, eval_wsd_preds)


def get_single_preds(args, model, tokenizer):
	wsd_eval_data = load_single_data(args.samples_data_path, max_context_words_count=args.max_context_words_count)

	return get_wsd_preds(args, wsd_eval_data, model, tokenizer)


def evaluate_model(args):
	print('Getting WSD results for \"{}\"...'.format(args.samples_data_path))

	'''
	LOAD TRAINED MODEL
	'''
	model = BiEncoderModel(args.encoder_name, freeze_gloss=args.freeze_gloss, freeze_context=args.freeze_context)
	if args.path_to_model is not None:
		if args.epoch_model_format:
			checkpoint = torch.load(args.path_to_model, map_location='cuda:0')
			model.load_state_dict(checkpoint['model'])
		else:
			model.load_state_dict(torch.load(args.path_to_model, map_location='cuda:0'))
	model = model.to(device)

	'''
	LOAD TOKENIZER
	'''
	tokenizer = load_tokenizer(args.encoder_name)

	'''
	LOAD EVAL SET
	'''
	def get_preds(data_format):
		if data_format == 'wic':
			return get_wic_preds(args, model, tokenizer)
		if data_format == 'single':
			return get_single_preds(args, model, tokenizer)
		raise RuntimeError(f'{data_format} format is not supported')

	eval_preds = get_preds(args.samples_data_format)

	#generate predictions file
	_, samples_filename = os.path.split(args.samples_data_path)
	pred_filepath = os.path.join(args.output_directory, f'preds_{samples_filename}')
	with open(pred_filepath, 'w') as f:
		json.dump(eval_preds, f, indent=4)

	#run predictions through scorer
	# gold_filepath = os.path.join(eval_path, '{}.gold.key.txt'.format(args.split))
	# scorer_path = os.path.join(args.data_path, 'Evaluation_Datasets')
	# p, r, f1 = evaluate_output(scorer_path, gold_filepath, pred_filepath)
	# print('f1 of BERT probe on {} test set = {}'.format(args.split, f1))

if __name__ == "__main__":
	# if not torch.cuda.is_available():
	# 	print("Need available GPU(s) to run this model...")
	# 	quit()

	#parse args
	args = parser.parse_args()
	print(args)

	#set random seeds
	torch.manual_seed(args.rand_seed)
	os.environ['PYTHONHASHSEED'] = str(args.rand_seed)
	torch.cuda.manual_seed(args.rand_seed)
	torch.cuda.manual_seed_all(args.rand_seed)
	np.random.seed(args.rand_seed)
	random.seed(args.rand_seed)
	torch.backends.cudnn.benchmark = False
	torch.backends.cudnn.deterministic = True

	evaluate_model(args)

#EOF